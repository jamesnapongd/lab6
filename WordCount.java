
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * 
 * @author Napong Dungduangsasitorn
 *
 */

public class WordCount {

	private Map<String, Integer> wordMap = new HashMap<String, Integer>();
	
	/**
	 * add word to check word that have in map or not.
	 * @param word 
	 */
	public void addWord(String word){
		String wordAdd = word.toLowerCase();
		
		if(wordMap.containsKey(wordAdd)){
			wordMap.replace(wordAdd, wordMap.get(wordAdd)+1);
		}
		else wordMap.put(wordAdd, 1);

	}
	/**
	 * get word method.
	 * @return wordSet contain set of word.
	 */
	public Set<String> getWords(){
		Set<String> wordSet = wordMap.keySet();
		return wordSet;

	}

	/**
	 * count word method.
	 * @param word
	 * @return value of word.
	 */
	public int getCount(String word){
		if(wordMap.containsKey(word.toLowerCase())){
			return wordMap.get(word);
		}
		return 0;

	}

	/**
	 * method sort word
	 * @return wordSort that sort word
	 */
	public String[] getSortedWord(){
		List<String> wordList = new ArrayList<String>(getWords());
		Collections.sort(wordList);
		String[] wordSort = new String[wordList.size()];
		wordList.toArray(wordSort);
		return wordSort;

	}

	

}
